package ud.example.gorditosybonitos.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ud.example.gorditosybonitos.R;
import ud.example.gorditosybonitos.home.articulos.ArticulosActivity;
import ud.example.gorditosybonitos.home.mapa.MapStoreActivity;
import ud.example.gorditosybonitos.webService.Store;
import ud.example.gorditosybonitos.webService.WebService;

public class HomeActivity extends AppCompatActivity {

    RecyclerView recyclerStore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        recyclerStore = findViewById(R.id.recyclerStore);
        ServerConnection server = new ServerConnection();
        server.execute();
    }


    public void setList(ArrayList<Store> lista){
        recyclerStore.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_store, parent, false));
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ViewHolder holder1 = (ViewHolder)holder;
                holder1.textViewName.setText(lista.get(position).name);
                holder1.textViewAddress.setText(lista.get(position).address);
                holder1.textViewPhone.setText(lista.get(position).phone);
                holder1.imageMapa.setOnClickListener(view -> {
                    Intent mapa = new Intent(HomeActivity.this,
                            MapStoreActivity.class);
                    mapa.putExtra("store", lista.get(position));
                    startActivity(mapa);
                });
                holder1.imageArticle.setOnClickListener(view -> {
                    Intent articulos = new Intent(HomeActivity.this,
                            ArticulosActivity.class);
                    articulos.putExtra("store", lista.get(position));
                    startActivity(articulos);
                });
                Picasso.get().load(lista.get(position).image).fit().centerCrop().into(holder1.imageStore);
            }

            @Override
            public int getItemCount() {
                return lista.size();
            }
        });

    }

    private class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewName;
        TextView textViewAddress;
        TextView textViewPhone;
        ImageView imageMapa;
        ImageView imageArticle;
        ImageView imageStore;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            textViewPhone = itemView.findViewById(R.id.textViewPhone);
            imageMapa = itemView.findViewById(R.id.imageMapa);
            imageArticle = itemView.findViewById(R.id.imageArticle);
            imageStore = itemView.findViewById(R.id.imageStore);

        }
    }


    private class ServerConnection extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            ArrayList<Store> lista = WebService.getStore();
            runOnUiThread(() -> setList(lista));

            return null;
        }
    }

}