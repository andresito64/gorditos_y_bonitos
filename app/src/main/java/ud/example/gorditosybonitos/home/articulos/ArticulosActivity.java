package ud.example.gorditosybonitos.home.articulos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import ud.example.gorditosybonitos.R;
import ud.example.gorditosybonitos.webService.Article;
import ud.example.gorditosybonitos.webService.WebService;

public class ArticulosActivity extends AppCompatActivity {

    RecyclerView recyclerArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulos);
        recyclerArticle = findViewById(R.id.recyclerArticle);
        ArticleConnection server = new ArticleConnection();
        server.execute();
    }

    public void setList(ArrayList<Article> lista){
        recyclerArticle.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new ArticleHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_articulo, parent, false));
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
               ArticleHolder holder1 = (ArticleHolder) holder;
                holder1.textViewName.setText(lista.get(position).name);
                holder1.textViewPrice.setText(lista.get(position).precio);
                holder1.textViewDesc.setText(lista.get(position).desc);
                Picasso.get().load(lista.get(position).image).fit().centerCrop().into(holder1.imageArticle);
            }

            @Override
            public int getItemCount() {
                return lista.size();
            }
        });

    }

    private class ArticleHolder extends RecyclerView.ViewHolder{
        TextView textViewName;
        TextView textViewPrice;
        TextView textViewDesc;
        ImageView imageArticle;
        public ArticleHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewDesc = itemView.findViewById(R.id.textViewDescription);
            imageArticle = itemView.findViewById(R.id.imageArticle);

        }
    }




    private class ArticleConnection extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            ArrayList<Article> lista = WebService.getArticles();
            runOnUiThread(() -> setList(lista));
            return null;
        }
    }
}