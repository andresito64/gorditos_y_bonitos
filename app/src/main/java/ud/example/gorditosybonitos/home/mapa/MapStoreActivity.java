package ud.example.gorditosybonitos.home.mapa;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ud.example.gorditosybonitos.R;
import ud.example.gorditosybonitos.webService.Store;

public class MapStoreActivity extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;

    private Store store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        store = (Store) getIntent().getSerializableExtra("store");
        setContentView(R.layout.activity_map_store);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    public void camSatelite(View v) {
        Button buttonType = (Button) v;
        int type = mMap.getMapType();
        switch (type){
            case 0 :
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                buttonType.setText("Normal");
                break;
            case 1 :
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                buttonType.setText("Satelite");
                break;
            case 2 :
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                buttonType.setText("Tierra");
                break;
            case 3 :
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                buttonType.setText("Hibrido");
                break;
            case 4 :
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                buttonType.setText("Ninguno");
                break;
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng current = new LatLng(store.latitude, store.longitud);
        mMap.addMarker(new MarkerOptions().position(current).title(store.name + " " + store.address));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 18));


    }
}