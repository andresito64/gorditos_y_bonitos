package ud.example.gorditosybonitos.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import ud.example.gorditosybonitos.R;
import ud.example.gorditosybonitos.home.HomeActivity;
import ud.example.gorditosybonitos.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText usuario;
    EditText password;
    Button login;
    Button registrar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usuario = findViewById(R.id.editTextUsuario);
        password = findViewById(R.id.editTextPassword);
        login = findViewById(R.id.buttonLogin);
        registrar = findViewById(R.id.buttonRegister);
        login.setOnClickListener(this);
        registrar.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view == login){
            if(usuario.getText().toString().isEmpty()){
                Toast.makeText(this, "Por favor digite el usuario", Toast.LENGTH_SHORT).show();
                return;
            }
            if(password.getText().toString().isEmpty()){
                Toast.makeText(this, "Por favor digite el password", Toast.LENGTH_SHORT).show();
                return;
            }

            Intent home = new Intent(this, HomeActivity.class);
            startActivity(home);
        }
        else{
            //Registro
            Intent register = new Intent(this, RegisterActivity.class);
            startActivity(register);
        }
    }


}