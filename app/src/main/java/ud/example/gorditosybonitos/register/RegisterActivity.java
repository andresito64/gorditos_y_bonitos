package ud.example.gorditosybonitos.register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ud.example.gorditosybonitos.R;
import ud.example.gorditosybonitos.home.HomeActivity;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Button register;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        register = findViewById(R.id.buttonRegister);
        register.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        Intent home = new Intent(this, HomeActivity.class);
        startActivity(home);

    }
}