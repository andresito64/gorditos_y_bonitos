package ud.example.gorditosybonitos.webService;

import org.ksoap2.serialization.SoapPrimitive;

import java.util.ArrayList;
import java.util.List;

public class Article {
    public String name;
    public String precio;
    public String desc;
    public String image;
    public Article(String name,
                          String precio,
                          String desc,
                          String image){
        this.name = name;
        this.precio = precio;
        this.desc = desc;
        this.image = image;
    }

    static ArrayList<Article> convertArray(List<SoapPrimitive> list){
        ArrayList listOut = new ArrayList<Store>();
        for (SoapPrimitive soap:
                list) {
            String text = soap.toString();
            String[] objetos = text.split(",");
            listOut.add(new Article(objetos[0],
                    objetos[1],
                    objetos[2],
                    objetos[3]));
        }
        return  listOut;
    }

    static ArrayList<Article> getArray(List<String> list){
        ArrayList listOut = new ArrayList<Store>();
        for (String text:
                list) {
            String[] objetos = text.split(",");
            listOut.add(new Article(objetos[0],
                    objetos[1],
                    objetos[2],
                    objetos[3]));
        }
        return  listOut;
    }


    static public List<String> getArticle() {
        List array = new ArrayList<String>();
        array.add("Computador,$2'000.000,Esta es una computador,https://media-cdn.tripadvisor.com/media/photo-s/14/11/4d/42/fachada-principal-centro.jpg");
        array.add("Celular,$2'000.000 ,El mejor celular que puedes comprar,https://files.rcnradio.com/public/styles/image_834x569/public/2020-05/centro_comercial_cacique_1_1_0.jpg?itok=JQ1EshkL");
        array.add("Tablet,$2'000.000, un ipad de ultima generacion,https://vivirenelpoblado.com/wp-content/uploads/parque-llerras-696x464.jpg");
        array.add("Calculadora,$2'000.000, Calculadora casio ,https://images.adsttc.com/media/images/55e6/5c0e/8450/b5a3/ae00/065e/medium_jpg/2-parque-bolivar-vista-aerea-hacia-el-oriente.jpg?1441160201");
        array.add("Monitor ,$2'000.000, Monitor Led Samsung,https://i.pinimg.com/564x/24/88/fd/2488fdd70db54b0a4eb516386d94be22.jpg");
        return array;
    }
}
