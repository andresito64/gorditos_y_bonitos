package ud.example.gorditosybonitos.webService;

import org.ksoap2.serialization.SoapPrimitive;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Store implements Serializable {
    public String name;
    public String address;
    public String phone;
    public Double latitude;
    public Double longitud;
    public String image;
    public Store(String name,
                 String address,
                 String phone,
                 Double latitud,
                 Double longitud,
                 String image){
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.latitude = latitud;
        this.longitud = longitud;
        this.image = image;
    }

    static ArrayList<Store> convertArray(List<SoapPrimitive> list){
        ArrayList listOut = new ArrayList<Store>();
        for (SoapPrimitive soap:
             list) {
            String text = soap.toString();
            String[] objetos = text.split(",");
            listOut.add(new Store(objetos[0],
                    objetos[1],
                    objetos[2],
                    Double.parseDouble(objetos[3]),
                    Double.parseDouble(objetos[4]),
                    objetos[5]));
        }
        return  listOut;
    }

    static ArrayList<Store> getArray(List<String> list){
        ArrayList listOut = new ArrayList<Store>();
        for (String text:
                list) {
            String[] objetos = text.split(",");
            listOut.add(new Store(objetos[0],
                    objetos[1],
                    objetos[2],
                    Double.parseDouble(objetos[3]),
                    Double.parseDouble(objetos[4]),
                    objetos[5]));
        }
        return  listOut;
    }


    static public List<String> getStore() {
        List array = new ArrayList<String>();
        array.add("Sede Bogotá,Carrera 19 # 88 – 12,7512356, 4.674930556, -74.05641389,https://media-cdn.tripadvisor.com/media/photo-s/14/11/4d/42/fachada-principal-centro.jpg");
        array.add("Sede Bucaramanga,Calle 19 # 20 – 14 ,5243124, 7.129333333, -73.12535556,https://files.rcnradio.com/public/styles/image_834x569/public/2020-05/centro_comercial_cacique_1_1_0.jpg?itok=JQ1EshkL");
        array.add("Sede Medellín,Calle 40 # 9 - 53,54126398, 6.20905, -75.56878889,https://vivirenelpoblado.com/wp-content/uploads/parque-llerras-696x464.jpg");
        array.add("Sede Santa Martha,Calle 17 # 2 – 60,7851245, 11.24336667, -74.21299167,https://images.adsttc.com/media/images/55e6/5c0e/8450/b5a3/ae00/065e/medium_jpg/2-parque-bolivar-vista-aerea-hacia-el-oriente.jpg?1441160201");
        array.add("Sede Manizales,Av. Paralela # 28 – 26,6528745, 5.063927778, -75.50249444,https://i.pinimg.com/564x/24/88/fd/2488fdd70db54b0a4eb516386d94be22.jpg");


        return array;
    }
}
