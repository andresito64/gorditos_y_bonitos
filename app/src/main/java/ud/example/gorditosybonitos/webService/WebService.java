package ud.example.gorditosybonitos.webService;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WebService {
    static String URL = "http://192.168.20.96:8080/FinalProject/StoreBuilder?WSDL";
    static  String NAME_SPACE = "http://example.ud/";

    public static ArrayList<Store> getStore(){
        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        SoapObject request = new SoapObject(NAME_SPACE, "getStore");
        envelope.setOutputSoapObject(request);
        try {
            httpTransportSE.call("",envelope);

            List<SoapPrimitive> response = (List<SoapPrimitive>) envelope.getResponse();
            return Store.convertArray(response);
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
            return Store.getArray(Store.getStore());
        }
    }

    public static ArrayList<Article> getArticles(){
        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        SoapObject request = new SoapObject(NAME_SPACE, "getArticle");
        envelope.setOutputSoapObject(request);
        try {
            httpTransportSE.call("",envelope);
            List<SoapPrimitive> response = (List<SoapPrimitive>) envelope.getResponse();
            return Article.convertArray(response);
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
            return Article.getArray(Article.getArticle());
        }
    }

}



